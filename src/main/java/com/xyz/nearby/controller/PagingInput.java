package com.xyz.nearby.controller;

import org.springframework.data.domain.Sort;

import java.util.List;

public class PagingInput {
    private Integer page = 0;
    private Integer pageSize = 5;
    private Sort.Direction sortDir = null;

    private List<String> sortBy = null;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Sort.Direction getSortDir() {
        return sortDir;
    }

    public void setSortDir(Sort.Direction sortDir) {
        this.sortDir = sortDir;
    }

    public List<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(List<String> sortBy) {
        this.sortBy = sortBy;
    }
}
