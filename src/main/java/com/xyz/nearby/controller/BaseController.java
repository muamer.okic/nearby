package com.xyz.nearby.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public abstract class BaseController {
    Integer DEFAULT_PAGE_SIZE = 10;
    public PageRequest getPageRequest(PagingInput pagingInput) {
        Integer pageSize = pagingInput.getPageSize();
        Sort.Direction sortDir = pagingInput.getSortDir();

        Sort sortObj = null;

        if (pagingInput.getSortBy() != null && !pagingInput.getSortBy().isEmpty()) {
            if (sortDir == null) {
                sortDir = Sort.Direction.ASC;
            }

            sortObj = Sort.by(sortDir, pagingInput.getSortBy().toString());
        }

        if (pageSize <= 0) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        if (sortObj == null) {
            return PageRequest.of(pagingInput.getPage(), pageSize);
        }

        return PageRequest.of(pagingInput.getPage(), pageSize, sortObj);
    }

    public void createPagingHeader(HttpServletResponse response, Page page) {
        response.setHeader("content-items", String.valueOf(page.getTotalElements()));
        response.setHeader("total-pages", String.valueOf(page.getTotalPages()));
        response.setHeader("current-page", String.valueOf(page.getNumber()));
        response.setHeader("page-size", String.valueOf(page.getSize()));
        response.setHeader("Access-Control-Allow-Headers", "content-items, total-pages, current-page, page-size, sort");
    }
}
