package com.xyz.nearby.controller;

import com.xyz.nearby.entity.Category;
import com.xyz.nearby.exceptions.CategoryNotFoundException;
import com.xyz.nearby.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    CategoryController(CategoryService categoryService) { this.categoryService = categoryService; }

    @PostMapping
    Category createCategory(Category category) {
        return categoryService.createCategory(category);
    }

    @GetMapping
    List<Category> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @PutMapping
    Category updateCategory(Category category) throws CategoryNotFoundException {
        return categoryService.updateCategory(category);
    }

    @DeleteMapping("/{categoryId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long categoryId) throws CategoryNotFoundException {
        categoryService.deleteCategory(categoryId);
    }
}
