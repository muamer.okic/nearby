package com.xyz.nearby.controller;

import com.xyz.nearby.entity.Product;
import com.xyz.nearby.exceptions.ProductNotFoundException;
import com.xyz.nearby.service.ProductService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController extends BaseController {

    private final ProductService productService;

    ProductController(ProductService service) {
        this.productService = service;
    }

    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    @GetMapping
    public List<Product> getProducts(PagingInput pagingInput, HttpServletResponse response) {
        PageRequest pageRequest = getPageRequest(pagingInput);
        Page<Product> products = productService.getAllProducts(pageRequest);
        createPagingHeader(response, products);
        return products.getContent();
    }

    @GetMapping("/coordinates")
    public List<Product> getNearByProducts(PagingInput pagingInput, @RequestParam Double latitude, @RequestParam Double longitude, HttpServletResponse response) {
        PageRequest pageRequest = getPageRequest(pagingInput);
        Page<Product> products = productService.getAllNearByProducts(pageRequest, latitude, longitude);
        createPagingHeader(response, products);
        return products.getContent();
    }

    @GetMapping("/{productId}")
    public Product getProduct(@PathVariable Long productId) throws ProductNotFoundException {
        return productService.getProductById(productId);
    }

    @PutMapping
    public Product updateProduct(@RequestBody Product product) throws ProductNotFoundException {
        return productService.updateProduct(product);
    }

    @DeleteMapping("/{productId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long productId) throws ProductNotFoundException {
         productService.deleteProduct(productId);
    }
}
