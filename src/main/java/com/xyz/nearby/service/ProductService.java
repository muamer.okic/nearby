package com.xyz.nearby.service;

import com.xyz.nearby.entity.Product;
import com.xyz.nearby.exceptions.ProductNotFoundException;
import com.xyz.nearby.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    ProductService(ProductRepository repository) {
        this.productRepository = repository;
    }

    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Product product) throws ProductNotFoundException {
        Product existingProduct = verifyProduct(product.getProductId());
        existingProduct.fromProduct(product);
        return productRepository.save(existingProduct);
    }

    public Product getProductById(Long productId) throws ProductNotFoundException {
        Product existingProduct = verifyProduct(productId);
        existingProduct.setViews(existingProduct.getViews() + 1);
        updateProduct(existingProduct);
        return existingProduct;
    }

    public Page<Product> getAllProducts(PageRequest pageRequest) {
        return productRepository.getAllProducts(pageRequest);
    }

    public Page<Product> getAllNearByProducts(PageRequest pageRequest, Double latitude, Double longitude) {
        return productRepository.getAllNearByProducts(pageRequest, latitude, longitude);
    }

    public void deleteProduct(Long productId) throws ProductNotFoundException {
        Product existingProduct = verifyProduct(productId);
        productRepository.delete(existingProduct);
    }

    private Product verifyProduct(Long productId) throws ProductNotFoundException {
        Product existingProduct = productRepository.getProductById(productId);
        if (existingProduct != null) {
            return existingProduct;
        }
        throw new ProductNotFoundException();
    }

}
