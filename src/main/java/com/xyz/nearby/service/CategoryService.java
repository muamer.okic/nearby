package com.xyz.nearby.service;

import com.xyz.nearby.entity.Category;
import com.xyz.nearby.exceptions.CategoryNotFoundException;
import com.xyz.nearby.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    CategoryService(CategoryRepository repository) { this.categoryRepository = repository; }

    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Category updateCategory(Category category) throws CategoryNotFoundException {
        Category existingCategory = verifyCategory(category.getCategoryId());
        return categoryRepository.save(existingCategory);
    }

    public void deleteCategory(Long categoryId) throws CategoryNotFoundException {
        Category existingCategory = verifyCategory(categoryId);
        categoryRepository.delete(existingCategory);
    }

    private Category verifyCategory(Long categoryId) throws CategoryNotFoundException {
        Category existingCategory = categoryRepository.getCategoryById(categoryId);
        if (existingCategory != null) {
            return existingCategory;
        }
        throw new CategoryNotFoundException();
    }

}
