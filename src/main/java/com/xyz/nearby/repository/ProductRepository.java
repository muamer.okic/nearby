package com.xyz.nearby.repository;

import com.xyz.nearby.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface ProductRepository extends BaseRepository<Product, Long> {

    @Query(value = "SELECT p FROM Product p",
            countQuery = "SELECT count(p.productId) FROM Product p")
    Page<Product> getAllProducts(Pageable pageable);

    @Query(value = "select p.*, haversine_formula(p.latitude, p.longitude, cast(:latitude as numeric), cast(:longitude as numeric)) as distance from product p order by distance",
            nativeQuery = true,
            countQuery = "SELECT count(p.productId) FROM Product p")
    Page<Product> getAllNearByProducts(Pageable pageable, Double latitude, Double longitude);

    @Query(value = "SELECT p FROM Product p WHERE p.productId=:id")
    Product getProductById(Long id);
}
