package com.xyz.nearby.repository;

import com.xyz.nearby.entity.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends BaseRepository<Category, Long> {
    @Query(value = "SELECT c FROM Category c WHERE c.categoryId=:id")
    Category getCategoryById(Long id);
}
