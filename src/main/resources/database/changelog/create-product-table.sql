--liquibase formatted sql
--changeset muamer.okic:create-product-table logicalFilePath:database/changelog/create-product-table.sql

CREATE TABLE IF NOT EXISTS product(
    product_id serial primary key not null,
    name varchar(120) not null,
    description varchar(255),
    category_id bigint not null,
    price float not null,
    longitude numeric not null,
    latitude numeric not null,
    views int,
    image varchar,
    constraint fk_category_id foreign key(category_id) references category(category_id)
    on update cascade on delete cascade
);

CREATE FUNCTION haversine_formula(
    lat1 NUMERIC,
    lon1 NUMERIC,
    lat2 NUMERIC,
    lon2 NUMERIC
) RETURNS NUMERIC AS '
    BEGIN
    RETURN 6371 * 2 * ATAN2(SQRT(SIN(RADIANS(lat2 - lat1) / 2) * SIN(RADIANS(lat2 - lat1) / 2) +
                                COS(RADIANS(lat1)) * COS(RADIANS(lat2)) *
                                SIN(RADIANS(lon2 - lon1) / 2) * SIN(RADIANS(lon2 - lon1) / 2)),
                       SQRT(1 - SIN(RADIANS(lat2 - lat1) / 2) * SIN(RADIANS(lat2 - lat1) / 2) +
                            COS(RADIANS(lat1)) * COS(RADIANS(lat2)) *
                            SIN(RADIANS(lon2 - lon1) / 2) * SIN(RADIANS(lon2 - lon1) / 2)));
    END;' LANGUAGE plpgsql;

--