--liquibase formatted sql
--changeset muamer.okic:create-category-table logicalFilePath:database/changelog/create-category-table.sql

CREATE TABLE IF NOT EXISTS category(
    category_id serial primary key not null,
    name varchar(120) not null
    );

--