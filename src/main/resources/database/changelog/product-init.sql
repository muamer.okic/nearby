--liquibase formatted sql
--changeset muamer.okic:product-init logicalFilePath:database/changelog/product-init.sql

INSERT INTO category(name) VALUES ('category1');
INSERT INTO category(name) VALUES ('category2');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
                    ('product1', 'description', 1, 30.5, 18.6671, 44.5384, 10, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
    ('product2', 'description', 1, 30.5, 18.6671, 44.5384, 13, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
    ('product3', 'description', 1, 30.5, 18.6671, 44.5384, 13, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
    ('product4', 'description', 1, 30.5, 18.6671, 44.5384, 13, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
    ('product5', 'description', 1, 30.5, 18.6671, 44.5384, 13, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
INSERT INTO product(name, description, category_id, price, longitude, latitude, views, image) VALUES
    ('product6', 'description', 1, 30.5, 18.6671, 44.5384, 13, 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png');
--