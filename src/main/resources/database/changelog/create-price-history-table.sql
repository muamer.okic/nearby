--liquibase formatted sql
--changeset muamer.okic:create-price-history-table logicalFilePath:database/changelog/create-price-history-table.sql

CREATE TABLE IF NOT EXISTS price_history(
    price_history_id serial primary key not null,
    product_id bigint not null,
    price float not null,
    timestamp timestamp with time zone default now(),
    constraint fk_product_id foreign key(product_id) references product(product_id)
    on update cascade on delete cascade
);

--