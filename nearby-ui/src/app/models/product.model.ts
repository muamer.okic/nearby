import {CategoryModel} from "./category.model";

export interface ProductModel {
  productId: number;
  name: string;
  description: string;
  categoryId: number;
  price: number;
  longitude: number;
  latitude: number;
  views: number;
  image: string;
  category: CategoryModel;
}
