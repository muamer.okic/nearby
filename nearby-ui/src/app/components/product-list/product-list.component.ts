import {Component, OnInit} from "@angular/core";
import {ProductModel} from "../../models/product.model";
import {ProductService} from "../../services/product.service";

@Component({
  selector: 'product-list',
  templateUrl: './product-list.template.html'
})
export class ProductListComponent implements OnInit {

  products : ProductModel[] = []
  pageNumber = 0

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getProducts(this.pageNumber).subscribe(response => this.products = response)
  }

}
