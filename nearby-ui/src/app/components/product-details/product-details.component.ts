import {Component, Input, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {ProductModel} from "../../models/product.model";
import {CategoryModel} from "../../models/category.model";

@Component({
  selector: 'product-details',
  templateUrl: './product-details.template.html'
})
export class ProductDetailsComponent implements OnInit {

  productId: number = 0;
  category: CategoryModel = {
    name: '',
    categoryId: 0
  }
  product: ProductModel = {
    productId: 0,
    name: '',
    description: '',
    categoryId: 0,
    price: 0,
    longitude: 0,
    latitude: 0,
    views: 0,
    image: '',
    category: this.category,
  }
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.productId = params['id']
      this.productService.getProduct(this.productId).subscribe(response => {
        this.product = response;
      })
    });
  }


}
