import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProductModel} from "../models/product.model";


@Injectable()
export class ProductService {

  baseUrl = "http://localhost:9040/products"
  constructor(private http: HttpClient) { }

  getProducts(pageNumber: number): Observable<ProductModel[]> {
    return this.http.get<ProductModel[]>(`${this.baseUrl}`, {
      params: {
        page: String(pageNumber)
      }
    });
  }

  getProduct(productId: number): Observable<ProductModel> {
    return this.http.get<ProductModel>(`${this.baseUrl}/${productId}`)
  }

}
