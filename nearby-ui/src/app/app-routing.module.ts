import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NgOptimizedImage} from "@angular/common";
import {ProductDetailsComponent} from "./components/product-details/product-details.component";
import {ProductListComponent} from "./components/product-list/product-list.component";

const routes: Routes = [
  {
    path: '',
    component: ProductListComponent,
    title: 'NearBy - Online marketplace'
  },
  {
    path: 'products/:id',
    component: ProductDetailsComponent,
    title: 'NearBy - Product'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), NgOptimizedImage],
  exports: [RouterModule]
})
export class AppRoutingModule { }

